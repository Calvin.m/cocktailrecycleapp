package com.rave.cocktailrecycleapp.model

import com.rave.cocktailrecycleapp.model.entity.CocktailCategory
import com.rave.cocktailrecycleapp.model.entity.Drink
import com.rave.cocktailrecycleapp.model.entity.DrinkDetails
import com.rave.cocktailrecycleapp.model.remote.CocktailService
import com.rave.cocktailrecycleapp.model.remote.dto.categorieslist.DrinkDTO
import com.rave.cocktailrecycleapp.model.remote.dto.drinkdetails.DrinkDetailsReponseDTO
import com.rave.cocktailrecycleapp.model.remote.dto.drinkslist.DrinksListResponseDTO
import javax.inject.Inject

/**
 * Cocktail repo.
 *
 * @property service
 * @constructor Create empty Cocktail repo
 */
class CocktailRepo @Inject constructor(private val service: CocktailService) {

    /**
     * Get cocktail categories.
     *
     * @return
     */
//    suspend fun getCocktailCategories(): Result<List<DrinkDTO>> {
    suspend fun getCocktailCategories(): List<CocktailCategory> {
        return if (service.getCocktailCategories().isSuccessful) {
            val cocktailsDTO = service.getCocktailCategories()
            val cocktails = cocktailsDTO.body()?.drinks?.map { it.toMapCocktail() } ?: emptyList()
//            Result.success(cocktails)
            cocktails
        } else {
//            Result.failure(throw java.lang.IllegalArgumentException("Error"))
            emptyList()
        }
    }

    private fun DrinkDTO.toMapCocktail(): CocktailCategory {
        return CocktailCategory(
            strCategory = strCategory
        )
    }

    /**
     * Get drinks in category.
     *
     * @param category
     * @return
     */
    suspend fun getDrinksInCategory(category: String): List<Drink> {
        val drinksResponse: DrinksListResponseDTO = service.getDrinksInCategory(category).body()
            ?: DrinksListResponseDTO()
        return drinksResponse.drinks.map {
            Drink(
                strDrink = it.strDrink ?: "",
                strDrinkThumb = it.strDrinkThumb
            )
        }
    }

    /**
     * Get drink details.
     *
     * @param drinkName
     * @return
     */
    suspend fun getDrinkDetails(drinkName: String): List<DrinkDetails> {
        val drinkDetailsResponse: DrinkDetailsReponseDTO = service.getDrinkDetails(drinkName).body()
            ?: DrinkDetailsReponseDTO()
        return drinkDetailsResponse.drinks.map {
            DrinkDetails(
                dateModified = it.dateModified ?: "",
                idDrink = it.idDrink ?: "",
                strAlcoholic = it.strAlcoholic ?: "",
                strCategory = it.strCategory ?: "",
                strCreativeCommonsConfirmed = it.strCreativeCommonsConfirmed ?: "",
                strDrink = it.strDrink ?: "",
                strDrinkAlternate = it.strDrinkAlternate ?: "",
                strDrinkThumb = it.strDrinkThumb ?: "",
                strGlass = it.strGlass ?: "",
                strIBA = it.strIBA ?: "",
                strImageAttribution = it.strImageAttribution ?: "",
                strImageSource = it.strImageSource ?: "",
                strIngredient1 = it.strIngredient1 ?: "",
                strIngredient10 = it.strIngredient10 ?: "",
                strIngredient11 = it.strIngredient11 ?: "",
                strIngredient12 = it.strIngredient12 ?: "",
                strIngredient13 = it.strIngredient13 ?: "",
                strIngredient14 = it.strIngredient14 ?: "",
                strIngredient15 = it.strIngredient15 ?: "",
                strIngredient2 = it.strIngredient2 ?: "",
                strIngredient3 = it.strIngredient3 ?: "",
                strIngredient4 = it.strIngredient4 ?: "",
                strIngredient5 = it.strIngredient5 ?: "",
                strIngredient6 = it.strIngredient6 ?: "",
                strIngredient7 = it.strIngredient7 ?: "",
                strIngredient8 = it.strIngredient8 ?: "",
                strIngredient9 = it.strIngredient9 ?: "",
                strInstructions = it.strInstructions ?: "",
                strInstructionsDE = it.strInstructionsDE ?: "",
                strInstructionsES = it.strInstructionsES ?: "",
                strInstructionsFR = it.strInstructionsFR ?: "",
                strInstructionsIT = it.strInstructionsIT ?: "",
                strInstructionsZHHANS = it.strInstructionsZHHANS ?: "",
                strInstructionsZHHANT = it.strInstructionsZHHANT ?: "",
                strMeasure1 = it.strMeasure1 ?: "",
                strMeasure10 = it.strMeasure10 ?: "",
                strMeasure11 = it.strMeasure11 ?: "",
                strMeasure12 = it.strMeasure12 ?: "",
                strMeasure13 = it.strMeasure13 ?: "",
                strMeasure14 = it.strMeasure14 ?: "",
                strMeasure15 = it.strMeasure15 ?: "",
                strMeasure2 = it.strMeasure2 ?: "",
                strMeasure3 = it.strMeasure3 ?: "",
                strMeasure4 = it.strMeasure4 ?: "",
                strMeasure5 = it.strMeasure5 ?: "",
                strMeasure6 = it.strMeasure6 ?: "",
                strMeasure7 = it.strMeasure7 ?: "",
                strMeasure8 = it.strMeasure8 ?: "",
                strMeasure9 = it.strMeasure9 ?: "",
                strTags = it.strTags ?: "",
                strVideo = it.strVideo ?: ""
            )
        }
    }
}
