package com.rave.cocktailrecycleapp.model.di

import com.rave.cocktailrecycleapp.model.remote.CocktailService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    @Singleton
    fun providesService(): CocktailService = CocktailService()
}
