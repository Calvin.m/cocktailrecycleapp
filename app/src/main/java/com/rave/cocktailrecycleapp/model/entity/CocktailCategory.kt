package com.rave.cocktailrecycleapp.model.entity

/**
 * DrinkDTO.
 *
 * @property strCategory
 * @constructor Create empty DrinkDTO
 */
data class CocktailCategory(
    val strCategory: String
)
