package com.rave.cocktailrecycleapp.model.entity

/**
 * Drink.
 *
 * @property strDrink
 * @property strDrinkThumb
 * @constructor Create empty Drink
 */
data class Drink(
    val strDrink: String,
    val strDrinkThumb: String
)
