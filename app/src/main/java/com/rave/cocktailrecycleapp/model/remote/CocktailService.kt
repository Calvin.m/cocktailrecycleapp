package com.rave.cocktailrecycleapp.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.rave.cocktailrecycleapp.model.remote.dto.categorieslist.CocktailCategoryResponseDTO
import com.rave.cocktailrecycleapp.model.remote.dto.drinkdetails.DrinkDetailsReponseDTO
import com.rave.cocktailrecycleapp.model.remote.dto.drinkslist.DrinksListResponseDTO
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Cocktail service.
 *
 * @constructor Create empty Cocktail service
 */
@OptIn(ExperimentalSerializationApi::class)
interface CocktailService {
    @GET(COCKTAIL_CATEGORIES_ENDPOINT)
    suspend fun getCocktailCategories(@Query("c") categoryList: String = "list"): Response<CocktailCategoryResponseDTO>

    @GET(COCKTAIL_DRINKS_IN_CATEGORY_ENDPOINT)
    suspend fun getDrinksInCategory(@Query("c") categoryName: String): Response<DrinksListResponseDTO>

    @GET(COCKTAIL_SELECTED_DRINK_DETAILS_ENDPOINT)
    suspend fun getDrinkDetails(@Query("s") drinkName: String): Response<DrinkDetailsReponseDTO>

    companion object {
        private const val COCKTAIL_CATEGORIES_ENDPOINT = "list.php"
        private const val COCKTAIL_DRINKS_IN_CATEGORY_ENDPOINT = "filter.php"
        private const val COCKTAIL_SELECTED_DRINK_DETAILS_ENDPOINT = "search.php"

        private const val BASE_URL = "https://www.thecocktaildb.com/api/json/v1/1/"

        /**
         * Invoke.
         *
         * @return
         */
        operator fun invoke(): CocktailService {
            val json = Json {
                ignoreUnknownKeys = true
            }
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                .build()
                .create()
        }
    }
}
