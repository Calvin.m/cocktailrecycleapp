package com.rave.cocktailrecycleapp.model.remote.dto.categorieslist

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CocktailCategoryResponseDTO(
    @SerialName("drinks")
    val drinks: List<DrinkDTO> = emptyList()
)
