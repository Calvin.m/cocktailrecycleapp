package com.rave.cocktailrecycleapp.model.remote.dto.categorieslist

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * DrinkDTO d t o.
 *
 * @property strCategory
 * @constructor Create empty DrinkDTO d t o
 */
@Serializable
data class DrinkDTO(
    @SerialName("strCategory")
    val strCategory: String
)
