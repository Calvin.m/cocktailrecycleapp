package com.rave.cocktailrecycleapp.model.remote.dto.drinkdetails

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DrinkDetailsReponseDTO(
    @SerialName("drinks")
    val drinks: List<DrinkDetailsDTO> = emptyList()
)
