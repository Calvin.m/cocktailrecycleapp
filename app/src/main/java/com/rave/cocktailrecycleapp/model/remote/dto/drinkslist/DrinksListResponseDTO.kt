package com.rave.cocktailrecycleapp.model.remote.dto.drinkslist

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DrinksListResponseDTO(
    @SerialName("drinks")
    val drinks: List<DrinkDTO> = emptyList()
)
