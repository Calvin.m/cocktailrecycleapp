package com.rave.cocktailrecycleapp.view.categories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.cocktailrecycleapp.databinding.CocktailItemsBindingBinding
import com.rave.cocktailrecycleapp.model.entity.CocktailCategory

/**
 * Cocktail adapter.
 *
 * @constructor Create empty Cocktail adapter
 */
class CocktailAdapter(
    private val cocktailClicked: (CocktailCategory) -> Unit
) : RecyclerView.Adapter<CocktailAdapter.CocktailViewHolder>() {

    private val cocktailCategories: MutableList<CocktailCategory> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CocktailViewHolder {
        return CocktailViewHolder.invoke(parent).apply {
            itemView.setOnClickListener {
                val cocktailSelected = cocktailCategories[adapterPosition]
                cocktailClicked(cocktailSelected)
            }
        }
    }

    override fun onBindViewHolder(holder: CocktailViewHolder, position: Int) {
        val cocktail = cocktailCategories[position]
        holder.bindCocktail(cocktail)
    }

    override fun getItemCount(): Int = cocktailCategories.size

    /**
     * Load categories.
     *
     * @param newCocktails
     */
    fun loadCategories(newCocktails: List<CocktailCategory>) {
        val startPosition = this.cocktailCategories.size
        this.cocktailCategories.addAll(newCocktails)
        notifyItemRangeInserted(startPosition, newCocktails.size)
    }

    /**
     * Cocktail view holder.
     *
     * @property binding
     * @constructor Create empty Cocktail view holder
     */
    class CocktailViewHolder(private val binding: CocktailItemsBindingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind cocktail.
         *
         * @param cocktail
         */
        fun bindCocktail(cocktail: CocktailCategory) {
            binding.cocktailCategoryName.text = cocktail.strCategory
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): CocktailViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = CocktailItemsBindingBinding.inflate(inflater, parent, false)
                return CocktailViewHolder(itemBinding)
            }
        }
    }
}
