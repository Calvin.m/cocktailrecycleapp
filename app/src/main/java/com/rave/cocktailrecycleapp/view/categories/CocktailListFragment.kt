package com.rave.cocktailrecycleapp.view.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.cocktailrecycleapp.R
import com.rave.cocktailrecycleapp.databinding.FragmentCocktailListBinding
import com.rave.cocktailrecycleapp.model.entity.CocktailCategory
import com.rave.cocktailrecycleapp.viewmodel.CocktailViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Cocktail list fragment.
 *
 * @constructor Create empty Cocktail list fragment
 */
@AndroidEntryPoint
class CocktailListFragment : Fragment() {
    private val cocktailViewModel by viewModels<CocktailViewModel>()
    private var _binding: FragmentCocktailListBinding? = null
    private val cocktailAdapter by lazy {
        CocktailAdapter { cocktail: CocktailCategory ->
            val args = bundleOf("categoryName" to cocktail.strCategory)
            findNavController().navigate(
                resId = R.id.action_FirstFragment_to_DrinkListFragment,
                args
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentCocktailListBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvCocktails.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@CocktailListFragment.cocktailAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cocktailViewModel.state.observe(viewLifecycleOwner) { state ->
            cocktailAdapter.loadCategories(state.cocktailList)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
