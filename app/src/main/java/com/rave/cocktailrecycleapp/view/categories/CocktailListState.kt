package com.rave.cocktailrecycleapp.view.categories

import com.rave.cocktailrecycleapp.model.entity.CocktailCategory

/**
 * Cocktail list state.
 *
 * @property isLoading
 * @property cocktailList
 * @constructor Create empty Cocktail list state
 */
data class CocktailListState(
    val isLoading: Boolean = false,
    val cocktailList: List<CocktailCategory> = emptyList()
)
