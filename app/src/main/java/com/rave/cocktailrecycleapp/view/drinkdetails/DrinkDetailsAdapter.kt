package com.rave.cocktailrecycleapp.view.drinkdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rave.cocktailrecycleapp.databinding.DrinkDetailsBindingBinding
import com.rave.cocktailrecycleapp.model.entity.DrinkDetails

/**
 * Drink details adapter.
 *
 * @constructor Create empty Drink details adapter
 */
class DrinkDetailsAdapter : RecyclerView.Adapter<DrinkDetailsAdapter.DrinkDetailsViewHolder>() {

    private val drinkDetails: MutableList<DrinkDetails> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkDetailsViewHolder {
        return DrinkDetailsViewHolder(parent)
    }

    override fun onBindViewHolder(holder: DrinkDetailsViewHolder, position: Int) {
        holder.bindDrinkDetails(drinkDetails[position])
    }

    override fun getItemCount(): Int = drinkDetails.size

    /**
     * Load drink details.
     *
     * @param newDrinks
     */
    fun loadDrinkDetails(newDrinks: List<DrinkDetails>) {
        val startPosition = this.drinkDetails.size
        if (newDrinks[0].strDrink.isNotEmpty()) {
            this.drinkDetails.addAll(newDrinks)
        }
        notifyItemRangeInserted(startPosition, newDrinks.size)
    }

    /**
     * Drink details view holder.
     *
     * @property binding
     * @constructor Create empty Drink details view holder
     */
    class DrinkDetailsViewHolder(private val binding: DrinkDetailsBindingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind drink details.
         *
         * @param drink
         */
        fun bindDrinkDetails(drink: DrinkDetails) {
            binding.drinkDetailsName.text = drink.strDrink
            binding.imageView.load(drink.strDrinkThumb)
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): DrinkDetailsViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = DrinkDetailsBindingBinding.inflate(inflater, parent, false)
                return DrinkDetailsViewHolder(itemBinding)
            }
        }
    }
}
