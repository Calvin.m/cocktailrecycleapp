package com.rave.cocktailrecycleapp.view.drinkdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.cocktailrecycleapp.databinding.FragmentDrinkDetailsBinding
import com.rave.cocktailrecycleapp.viewmodel.DrinkDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Drink details fragment.
 *
 * @constructor Create empty Drink details fragment
 */
@AndroidEntryPoint
class DrinkDetailsFragment : Fragment() {

    private val drinkDetailsViewModel by viewModels<DrinkDetailsViewModel>()
    private var _binding: FragmentDrinkDetailsBinding? = null
    private val drinkDetailsAdapter by lazy {
        DrinkDetailsAdapter()
    }
    private val drinkName by lazy { arguments?.getString("drinkName") }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        drinkDetailsViewModel.getDrinkDetails(drinkName ?: "")
        return FragmentDrinkDetailsBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvCocktails.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@DrinkDetailsFragment.drinkDetailsAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkDetailsViewModel.drinkDetailsState.observe(viewLifecycleOwner) { state ->
            drinkDetailsAdapter.loadDrinkDetails(listOf(state.drinkDetails))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
