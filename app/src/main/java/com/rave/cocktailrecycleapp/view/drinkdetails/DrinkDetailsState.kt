package com.rave.cocktailrecycleapp.view.drinkdetails

import com.rave.cocktailrecycleapp.model.entity.DrinkDetails

/**
 * Drink details state.
 *
 * @property isLoading
 * @property drinkDetails
 * @constructor Create empty Drink details state
 */
data class DrinkDetailsState(
    val isLoading: Boolean = false,
    val drinkDetails: DrinkDetails = DrinkDetails()
)
