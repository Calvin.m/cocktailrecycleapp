package com.rave.cocktailrecycleapp.view.drinklist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.cocktailrecycleapp.databinding.DrinkItemsBindingBinding
import com.rave.cocktailrecycleapp.model.entity.Drink

/**
 * Drink list adapter.
 *
 * @property drinkClicked
 * @constructor Create empty Drink list adapter
 */
class DrinkListAdapter(
    private val drinkClicked: (Drink) -> Unit
) : RecyclerView.Adapter<DrinkListAdapter.DrinkViewHolder>() {
    private val drinkList: MutableList<Drink> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder {
        return DrinkViewHolder.invoke(parent).apply {
            itemView.setOnClickListener {
                val drinkSelected = drinkList[adapterPosition]
                drinkClicked(drinkSelected)
            }
        }
    }

    override fun onBindViewHolder(holder: DrinkViewHolder, position: Int) {
        holder.bindDrink(drinkList[position])
    }

    override fun getItemCount(): Int = drinkList.size

    /**
     * Load drinks.
     *
     * @param newDrinks
     */
    fun loadDrinks(newDrinks: List<Drink>) {
        val startPosition = this.drinkList.size
        this.drinkList.addAll(newDrinks)
        notifyItemRangeInserted(startPosition, newDrinks.size)
    }

    /**
     * Drink view holder.
     *
     * @property binding
     * @constructor Create empty Drink view holder
     */
    class DrinkViewHolder(private val binding: DrinkItemsBindingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind drink.
         *
         * @param drink
         */
        fun bindDrink(drink: Drink) {
            binding.cocktailDrinkName.text = drink.strDrink
            // add ImageView in XML file
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): DrinkViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = DrinkItemsBindingBinding.inflate(inflater, parent, false)
                return DrinkViewHolder(itemBinding)
            }
        }
    }
}
