package com.rave.cocktailrecycleapp.view.drinklist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.cocktailrecycleapp.R
import com.rave.cocktailrecycleapp.databinding.FragmentDrinkListBinding
import com.rave.cocktailrecycleapp.viewmodel.DrinkListViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Drink list fragment.
 *
 * @constructor Create empty Drink list fragment
 */
@AndroidEntryPoint
class DrinkListFragment : Fragment() {
    private val drinksViewModel by viewModels<DrinkListViewModel>()
    private var _binding: FragmentDrinkListBinding? = null
    private val drinksAdapter by lazy {
        DrinkListAdapter { drink ->
            val args = bundleOf("drinkName" to drink.strDrink)

            findNavController().navigate(
                resId = R.id.action_DrinkListFragment_to_drinkDetailsFragment,
                args
            )
        }
    }

    private val categoryName by lazy { arguments?.getString("categoryName") }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        drinksViewModel.getDrinksInCategory(categoryName ?: "")
        return FragmentDrinkListBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvCocktails.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@DrinkListFragment.drinksAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinksViewModel.drinkListState.observe(viewLifecycleOwner) { state ->
            drinksAdapter.loadDrinks(state.drinksList)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
