package com.rave.cocktailrecycleapp.view.drinklist

import com.rave.cocktailrecycleapp.model.entity.Drink

/**
 * Drink list state.
 *
 * @property isLoading
 * @property drinksList
 * @constructor Create empty Drink list state
 */
data class DrinkListState(
    val isLoading: Boolean = false,
    val drinksList: List<Drink> = emptyList()
)
