package com.rave.cocktailrecycleapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.cocktailrecycleapp.model.CocktailRepo
import com.rave.cocktailrecycleapp.model.entity.CocktailCategory
import com.rave.cocktailrecycleapp.view.categories.CocktailListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Cocktail view model.
 *
 * @property repo
 * @constructor Create empty Cocktail view model
 */
@HiltViewModel
class CocktailViewModel @Inject constructor(
    private val repo: CocktailRepo
) : ViewModel() {

    private val _state = MutableLiveData(CocktailListState())
    val state: LiveData<CocktailListState> get() = _state

    init {
        fetchCocktails()
    }

    private fun fetchCocktails() {
        viewModelScope.launch {
            _state.isLoading(true)
            viewModelScope.launch {
                val list = repo.getCocktailCategories()
                _state.isSuccess(list)
            }
        }
    }

    private fun MutableLiveData<CocktailListState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<CocktailListState>.isSuccess(drinks: List<CocktailCategory>) {
        value = value?.copy(isLoading = false, cocktailList = drinks)
    }
}
