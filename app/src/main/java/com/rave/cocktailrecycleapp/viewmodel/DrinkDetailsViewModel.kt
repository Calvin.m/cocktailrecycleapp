package com.rave.cocktailrecycleapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.cocktailrecycleapp.model.CocktailRepo
import com.rave.cocktailrecycleapp.model.entity.DrinkDetails
import com.rave.cocktailrecycleapp.view.drinkdetails.DrinkDetailsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Drink details view model.
 *
 * @property repo
 * @constructor Create empty Drink details view model
 */
@HiltViewModel
class DrinkDetailsViewModel @Inject constructor(
    private val repo: CocktailRepo
) : ViewModel() {

    @Suppress("VariableNaming", "ObjectPropertyNaming")
    val _drinkDetailsState = MutableLiveData(DrinkDetailsState())
    val drinkDetailsState: LiveData<DrinkDetailsState> get() = _drinkDetailsState

    /**
     * Get drink details.
     *
     * @param drink
     */
    fun getDrinkDetails(drink: String) {
        viewModelScope.launch {
            _drinkDetailsState.isLoading(true)
            val drinkList = repo.getDrinkDetails(drink)
            _drinkDetailsState.isSuccess(drinkList)
        }
    }

    private fun MutableLiveData<DrinkDetailsState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<DrinkDetailsState>.isSuccess(drinks: List<DrinkDetails>) {
        value = value?.copy(
            isLoading = false,
            drinkDetails = DrinkDetails(
                strDrink = drinks[0].strDrink,
                strDrinkThumb = drinks[0].strDrinkThumb
            )
        )
    }
}
