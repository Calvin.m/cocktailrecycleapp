package com.rave.cocktailrecycleapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.cocktailrecycleapp.model.CocktailRepo
import com.rave.cocktailrecycleapp.model.entity.Drink
import com.rave.cocktailrecycleapp.view.drinklist.DrinkListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Drink list view model.
 *
 * @property repo
 * @constructor Create empty Drink list view model
 */
@HiltViewModel
class DrinkListViewModel @Inject constructor(
    private val repo: CocktailRepo
) : ViewModel() {

    private val _drinkListState = MutableLiveData(DrinkListState())
    val drinkListState: LiveData<DrinkListState> get() = _drinkListState

    /**
     * Get drinks in category.
     *
     * @param category
     */
    fun getDrinksInCategory(category: String) {
        viewModelScope.launch {
            _drinkListState.isLoading(true)
            val drinkList = repo.getDrinksInCategory(category)
            _drinkListState.isSuccess(drinkList)
        }
    }

    private fun MutableLiveData<DrinkListState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<DrinkListState>.isSuccess(drinks: List<Drink>) {
        value = value?.copy(isLoading = false, drinksList = drinks)
    }
}
