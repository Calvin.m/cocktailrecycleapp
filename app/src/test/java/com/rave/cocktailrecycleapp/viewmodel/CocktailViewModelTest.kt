package com.rave.cocktailrecycleapp.viewmodel

import com.rave.cocktailrecycleapp.model.CocktailRepo
import com.rave.cocktailrecycleapp.utilTest.CoroutinesTestExtension
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

class CocktailViewModelTest {

    @RegisterExtension
    val testExtension = CoroutinesTestExtension()

    private val repo: CocktailRepo = mockk()
    private val viewModel: CocktailViewModel = CocktailViewModel(repo)

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testInit() = runTest(testExtension.testDispatcher) {
        val state = viewModel.state.value
        Assertions.assertTrue(state?.isLoading == false)
//        Assertions.assertTrue(state?.cocktailList?.isEmpty() == true)
    }

    fun testValueChange() = runTest {
        // Given
//        coEvery { repo.getCocktailCategories() } coAnswers {  }
    }
}
