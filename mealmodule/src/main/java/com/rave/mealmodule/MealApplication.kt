package com.rave.mealmodule

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * MealDetailsDTO application.
 *
 * @constructor Create empty MealDetailsDTO application
 */
@HiltAndroidApp
class MealApplication : Application()
