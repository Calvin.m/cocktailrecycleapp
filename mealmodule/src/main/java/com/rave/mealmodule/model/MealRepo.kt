package com.rave.mealmodule.model

import android.util.Log
import com.rave.mealmodule.model.dto.areaslisting.AreaDTO
import com.rave.mealmodule.model.dto.ingredientlisting.IngredientDTO
import com.rave.mealmodule.model.dto.mealcategory.MealCategoryDTO
import com.rave.mealmodule.model.dto.mealdetails.MealDetailsDTO
import com.rave.mealmodule.model.dto.meals.MealDTO
import com.rave.mealmodule.model.entity.Area
import com.rave.mealmodule.model.entity.Ingredient
import com.rave.mealmodule.model.entity.Meal
import com.rave.mealmodule.model.entity.MealCategory
import com.rave.mealmodule.model.entity.MealDetails
import com.rave.mealmodule.model.remote.MealDatabase
import com.rave.mealmodule.model.remote.MealEntity
import com.rave.mealmodule.model.remote.MealService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Meal repo.
 *
 * @property service
 * @property db
 * @constructor Create empty Meal repo
 */
class MealRepo @Inject constructor(
    private val service: MealService,
    private val db: MealDatabase
) {

    val dao = db.mealDAO()

    /**
     * Get ingredients listing.
     *
     * @return
     */
    suspend fun getIngredientsListing(): List<Ingredient> {
        val ingredientListingDTO = service.getIngredientsListing()
        return ingredientListingDTO.ingredients.map { it.toMapIngredient() }
    }

    private fun IngredientDTO.toMapIngredient(): Ingredient {
        return Ingredient(
            idIngredient ?: "",
            strDescription ?: "",
            strIngredient ?: "",
            strType ?: ""
        )
    }

    /**
     * Get areas listing.
     *
     * @return
     */
    suspend fun getAreasListing(): List<Area> {
        val areaListingDTO = service.getAreasListing()
        return areaListingDTO.areas.map { it.toMapArea() }
    }

    private fun AreaDTO.toMapArea(): Area {
        return Area(
            strArea
        )
    }

    /**
     * Get meal categories.
     *
     * @return
     */
    suspend fun getMealCategories(): List<MealCategory> {
        val mealCategoryDto = service.getAllMealCategories()
        val categories =
            mealCategoryDto.categories.map { it.toMapCategory() }
        return categories
    }

    private fun MealCategoryDTO.toMapCategory(): MealCategory {
        return MealCategory(
            idCategory = this.idCategory,
            strCategory = this.strCategory,
            strCategoryDescription = this.strCategoryDescription,
            strCategoryThumb
        )
    }

    /**
     * Get meals.
     *
     * @param category
     * @return
     */
    suspend fun getMeals(category: String): List<Meal> {
        val mealsDto = service.getMealsInCategory(category)
        val meals = mealsDto.meals.map { it.toMapMeal() }
        return meals
    }

    /**
     * Get meals by area.
     *
     * @param area
     * @return
     */
    suspend fun getMealsByArea(area: String): List<Meal> {
        val mealsDto = service.getMealsInArea(area)
        val meals = mealsDto.meals.map { it.toMapMeal() }
        return meals
    }

    /**
     * Get meals by ingredient.
     *
     * @param ingredient
     * @return
     */
    suspend fun getMealsByIngredient(ingredient: String): List<Meal> {
        val mealsDto = service.getMealsInIngredient(ingredient)
        return mealsDto.meals.map { it.toMapMeal() }
    }

    private fun MealDTO.toMapMeal(): Meal {
        return Meal(
            idMeal = this.idMeal,
            strMeal = this.strMeal,
            strMealThumb
        )
    }

    /**
     * Add meal.
     *
     * @param meal
     */
    suspend fun addMeal(meal: MealEntity) = withContext(Dispatchers.IO) {
        dao.addMeal(meal)
    }

    /**
     * Delete meal.
     *
     * @param meal
     */
    suspend fun deleteMeal(meal: MealEntity) = withContext(Dispatchers.IO) {
        dao.deleteMeal(meal)
    }

    /**
     * Get favorited meals.
     *
     */
    suspend fun getFavoritedMeals() = withContext(Dispatchers.IO) {
        dao.getMeals()
    }

    /**
     * Get meal details.
     *
     * @param meal
     * @return
     */
    suspend fun getMealDetails(meal: String): List<MealDetails> {
        val mealDetailsDTO = service.getMeal(meal)
        val mealDetails = mealDetailsDTO.meals.map { it.toMapMealDetails() }
        return mealDetails
    }

    private fun MealDetailsDTO.toMapMealDetails(): MealDetails {
        return MealDetails(
            dateModified = this.dateModified ?: "",
            idMeal = this.idMeal ?: "",
            strArea = this.strArea ?: "",
            strCategory = this.strCategory ?: "",
            strCreativeCommonsConfirmed = this.strCreativeCommonsConfirmed ?: "",
            strDrinkAlternate = this.strDrinkAlternate ?: "",
            strImageSource = this.strImageSource ?: "",
            strIngredient1 = this.strIngredient1 ?: "",
            strIngredient10 = this.strIngredient10 ?: "",
            strIngredient11 = this.strIngredient11 ?: "",
            strIngredient12 = this.strIngredient12 ?: "",
            strIngredient13 = this.strIngredient13 ?: "",
            strIngredient14 = this.strIngredient14 ?: "",
            strIngredient15 = this.strIngredient15 ?: "",
            strIngredient16 = this.strIngredient16 ?: "",
            strIngredient17 = this.strIngredient17 ?: "",
            strIngredient18 = this.strIngredient18 ?: "",
            strIngredient19 = this.strIngredient19 ?: "",
            strIngredient2 = this.strIngredient2 ?: "",
            strIngredient20 = this.strIngredient20 ?: "",
            strIngredient3 = this.strIngredient3 ?: "",
            strIngredient4 = this.strIngredient4 ?: "",
            strIngredient5 = this.strIngredient5 ?: "",
            strIngredient6 = this.strIngredient6 ?: "",
            strIngredient7 = this.strIngredient7 ?: "",
            strIngredient8 = this.strIngredient8 ?: "",
            strIngredient9 = this.strIngredient9 ?: "",
            strInstructions = this.strInstructions ?: "",
            strMeal = this.strMeal ?: "",
            strMealThumb = this.strMealThumb ?: "",
            strMeasure1 = this.strMeasure1 ?: "",
            strMeasure10 = this.strMeasure10 ?: "",
            strMeasure11 = this.strMeasure11 ?: "",
            strMeasure12 = this.strMeasure12 ?: "",
            strMeasure13 = this.strMeasure13 ?: "",
            strMeasure14 = this.strMeasure14 ?: "",
            strMeasure15 = this.strMeasure15 ?: "",
            strMeasure16 = this.strMeasure16 ?: "",
            strMeasure17 = this.strMeasure17 ?: "",
            strMeasure18 = this.strMeasure18 ?: "",
            strMeasure19 = this.strMeasure19 ?: "",
            strMeasure2 = this.strMeasure2 ?: "",
            strMeasure20 = this.strMeasure20 ?: "",
            strMeasure3 = this.strMeasure3 ?: "",
            strMeasure4 = this.strMeasure4 ?: "",
            strMeasure5 = this.strMeasure5 ?: "",
            strMeasure6 = this.strMeasure6 ?: "",
            strMeasure7 = this.strMeasure7 ?: "",
            strMeasure8 = this.strMeasure8 ?: "",
            strMeasure9 = this.strMeasure9 ?: "",
            strSource = this.strSource ?: "",
            strTags = this.strTags ?: "",
            strYoutube = this.strYoutube ?: ""
        )
    }
}
