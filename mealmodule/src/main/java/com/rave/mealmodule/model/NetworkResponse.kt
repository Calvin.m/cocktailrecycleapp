package com.rave.mealmodule.model

/**
 * Network response.
 *
 * @param T
 * @constructor Create empty Network response
 */
sealed class NetworkResponse<T> {
    /**
     * Success.
     *
     * @param T
     * @property successList
     * @constructor Create empty Success
     */
    sealed class Success<T>(
        val successList: List<T>
    ) : NetworkResponse<List<T>>() {

        /**
         * Category success.
         *
         * @param MealCategory
         * @property categoryList
         * @constructor Create empty Category success
         */
        data class CategorySuccess<MealCategory>(val categoryList: List<MealCategory>) :
            Success<MealCategory>(categoryList)
    }

    /**
     * Error.
     *
     * @property message
     * @constructor Create empty Error
     */
    data class Error(val message: String) : NetworkResponse<String>()
}
