package com.rave.mealmodule.model.dto.areaslisting

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AreaDTO(
    @SerialName("strArea")
    val strArea: String
)
