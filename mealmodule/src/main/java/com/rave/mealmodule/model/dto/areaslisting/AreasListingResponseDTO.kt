package com.rave.mealmodule.model.dto.areaslisting

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AreasListingResponseDTO(
    @SerialName("meals")
    val areas: List<AreaDTO>
)
