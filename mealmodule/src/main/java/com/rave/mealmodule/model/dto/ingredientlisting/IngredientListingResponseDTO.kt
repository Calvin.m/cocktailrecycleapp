package com.rave.mealmodule.model.dto.ingredientlisting

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class IngredientListingResponseDTO(
    @SerialName("meals")
    val ingredients: List<IngredientDTO>
)
