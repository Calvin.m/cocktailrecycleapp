package com.rave.mealmodule.model.dto.mealcategory

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MealCategoryResponseDTO(
    @SerialName("categories")
    val categories: List<MealCategoryDTO> = emptyList()
)
