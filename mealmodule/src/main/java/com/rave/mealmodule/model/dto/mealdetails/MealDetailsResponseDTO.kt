package com.rave.mealmodule.model.dto.mealdetails

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MealDetailsResponseDTO(
    @SerialName("meals")
    val meals: List<MealDetailsDTO>
)
