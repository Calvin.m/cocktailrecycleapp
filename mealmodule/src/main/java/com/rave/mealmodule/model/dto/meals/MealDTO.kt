package com.rave.mealmodule.model.dto.meals

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MealDTO(
    @SerialName("idMeal")
    val idMeal: String,
    @SerialName("strMeal")
    val strMeal: String,
    @SerialName("strMealThumb")
    val strMealThumb: String
)
