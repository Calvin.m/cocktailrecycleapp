package com.rave.mealmodule.model.dto.meals

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MealsResponseDTO(
    @SerialName("meals")
    val meals: List<MealDTO>
)
