package com.rave.mealmodule.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class Area(
    val strArea: String
)
