package com.rave.mealmodule.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class MealCategory(
    val idCategory: String,
    val strCategory: String,
    val strCategoryDescription: String,
    val strCategoryThumb: String
)
