package com.rave.mealmodule.model.remote

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MealDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addMeal(meal: MealEntity)

    @Query("SELECT * FROM meals_table")
    suspend fun getMeals(): List<MealEntity>

    @Delete
    suspend fun deleteMeal(meal: MealEntity)
}
