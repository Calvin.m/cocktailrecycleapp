package com.rave.mealmodule.model.remote

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [MealEntity::class], version = 1)
abstract class MealDatabase : RoomDatabase() {
    abstract fun mealDAO(): MealDAO
}
