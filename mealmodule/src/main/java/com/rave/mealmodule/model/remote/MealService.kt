package com.rave.mealmodule.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.rave.mealmodule.model.dto.areaslisting.AreasListingResponseDTO
import com.rave.mealmodule.model.dto.ingredientlisting.IngredientListingResponseDTO
import com.rave.mealmodule.model.dto.mealcategory.MealCategoryResponseDTO
import com.rave.mealmodule.model.dto.mealdetails.MealDetailsResponseDTO
import com.rave.mealmodule.model.dto.meals.MealsResponseDTO
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Meal service.
 *
 * @constructor Create empty Meal service
 */
@OptIn(ExperimentalSerializationApi::class)
interface MealService {
    @GET(CATEGORY_ENDPOINT)
    suspend fun getAllMealCategories(): MealCategoryResponseDTO

    @GET(MEALS_IN_CATEGORY_ENDPOINT)
    suspend fun getMealsInCategory(@Query("c") categoryName: String): MealsResponseDTO

    @GET(MEAL_DETAILS_ENDPOINT)
    suspend fun getMeal(@Query("s") mealName: String): MealDetailsResponseDTO

    @GET(AREAS_LISTING_ENDPOINT)
    suspend fun getAreasListing(@Query("a") categoryName: String = "list"): AreasListingResponseDTO

    @GET(MEALS_IN_CATEGORY_ENDPOINT)
    suspend fun getMealsInArea(@Query("a") categoryName: String): MealsResponseDTO

    @GET(AREAS_LISTING_ENDPOINT)
    suspend fun getIngredientsListing(@Query("i") categoryName: String = "list"): IngredientListingResponseDTO

    @GET(MEALS_IN_CATEGORY_ENDPOINT)
    suspend fun getMealsInIngredient(@Query("i") categoryName: String): MealsResponseDTO

    companion object {
        private const val CATEGORY_ENDPOINT = "categories.php"
        private const val MEALS_IN_CATEGORY_ENDPOINT = "filter.php"
        private const val MEAL_DETAILS_ENDPOINT = "search.php"
        private const val AREAS_LISTING_ENDPOINT = "list.php"

        // https://www.themealdb.com/api/json/v1/1/categories.php

        private const val BASE_URL = "https://www.themealdb.com/api/json/v1/1/"

        /**
         * Invoke.
         *
         * @return
         */
        operator fun invoke(): MealService {
            val json = Json {
                ignoreUnknownKeys = true
                explicitNulls = true
            }
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                .build()
                .create()
        }
    }
}
