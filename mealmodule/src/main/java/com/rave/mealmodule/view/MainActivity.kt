package com.rave.mealmodule.view

import androidx.appcompat.app.AppCompatActivity
import com.rave.mealmodule.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)
