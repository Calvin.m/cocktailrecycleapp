package com.rave.mealmodule.view.arealisting

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.mealmodule.databinding.AreaItemBinding
import com.rave.mealmodule.model.entity.Area

/**
 * Area listing adapter.
 *
 * @property areaClicked
 * @constructor Create empty Area listing adapter
 */
class AreaListingAdapter(private val areaClicked: (Area) -> Unit) :
    RecyclerView.Adapter<AreaListingAdapter.AreaListingViewHolder>() {

    private val areaListing: MutableList<Area> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AreaListingViewHolder {
        return AreaListingViewHolder.invoke(parent)
            .apply {
                itemView.setOnClickListener {
                    val areaSelected = areaListing[adapterPosition]
                    areaClicked(areaSelected)
                }
            }
    }

    override fun onBindViewHolder(holder: AreaListingViewHolder, position: Int) {
        val area = areaListing[position]
        holder.bindArea(area)
    }

    override fun getItemCount(): Int = areaListing.size

    /**
     * Load area listing.
     *
     * @param newAreas
     */
    fun loadAreaListing(newAreas: List<Area>) {
        val oldSize = areaListing.size
        areaListing.clear()
        notifyItemRangeRemoved(0, oldSize)
        areaListing.addAll(newAreas)
        notifyItemRangeInserted(0, newAreas.size)
    }

    /**
     * Area listing view holder.
     *
     * @property binding
     * @constructor Create empty Area listing view holder
     */
    class AreaListingViewHolder(private val binding: AreaItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind area.
         *
         * @param area
         */
        fun bindArea(area: Area) {
            binding.areaName.text = area.strArea
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): AreaListingViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = AreaItemBinding.inflate(inflater, parent, false)
                return AreaListingViewHolder(itemBinding)
            }
        }
    }
}
