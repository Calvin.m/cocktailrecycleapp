package com.rave.mealmodule.view.arealisting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.mealmodule.R
import com.rave.mealmodule.databinding.FragmentAreaListingBinding
import com.rave.mealmodule.viewmodel.AreaListingViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Area listing fragment.
 *
 * @constructor Create empty Area listing fragment
 */
@AndroidEntryPoint
class AreaListingFragment : Fragment() {
    private val areaListingViewModel by viewModels<AreaListingViewModel>()
    private var _binding: FragmentAreaListingBinding? = null
    private val areaListingAdapter by lazy {
        AreaListingAdapter { area ->
            val args = bundleOf("areaName" to area.strArea)
            findNavController().navigate(
                resId = R.id.action_areaListingFragment_to_mealsFragment,
                args
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        areaListingViewModel.fetchAreas()
        return FragmentAreaListingBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvAreaListing.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@AreaListingFragment.areaListingAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        areaListingViewModel.state.observe(viewLifecycleOwner) { state ->
            areaListingAdapter.loadAreaListing(state.areaListing)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
