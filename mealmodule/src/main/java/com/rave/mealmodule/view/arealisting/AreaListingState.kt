package com.rave.mealmodule.view.arealisting

import com.rave.mealmodule.model.entity.Area

/**
 * Area listing state.
 *
 * @property isLoading
 * @property areaListing
 * @constructor Create empty Area listing state
 */
data class AreaListingState(
    val isLoading: Boolean = false,
    val areaListing: List<Area> = emptyList()
)
