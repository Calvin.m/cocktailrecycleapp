package com.rave.mealmodule.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rave.mealmodule.R
import com.rave.mealmodule.databinding.FragmentMealHomeBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meal home fragment.
 *
 * @constructor Create empty Meal home fragment
 */
@AndroidEntryPoint
class MealHomeFragment : Fragment() {

    private var _binding: FragmentMealHomeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentMealHomeBinding.inflate(inflater, container, false).apply {
            _binding = this
            categoriesbutton.setOnClickListener {
                findNavController().navigate(R.id.action_MealHomeFragment_to_mealCategoriesFragment)
            }
            areabutton.setOnClickListener {
                findNavController().navigate(R.id.action_MealHomeFragment_to_areaListingFragment)
            }
            ingredientsbutton.setOnClickListener {
                findNavController().navigate(R.id.action_MealHomeFragment_to_ingredientListingFragment)
            }
            savedmealsbutton.setOnClickListener {
                val args = bundleOf("fromHomeScreen" to "fromHomeScreen")
                findNavController().navigate(
                    resId = R.id.action_MealHomeFragment_to_mealDetailsFragment,
                    args
                )
            }
        }.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
