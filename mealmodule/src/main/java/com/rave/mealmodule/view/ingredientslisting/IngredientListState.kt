package com.rave.mealmodule.view.ingredientslisting

import com.rave.mealmodule.model.entity.Ingredient

/**
 * Ingredient list state.
 *
 * @property isLoading
 * @property ingredientList
 * @constructor Create empty Ingredient list state
 */
data class IngredientListState(
    val isLoading: Boolean = false,
    val ingredientList: List<Ingredient> = emptyList()
)
