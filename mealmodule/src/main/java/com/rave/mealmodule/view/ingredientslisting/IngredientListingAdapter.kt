package com.rave.mealmodule.view.ingredientslisting

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.mealmodule.databinding.IngredientItemBinding
import com.rave.mealmodule.model.entity.Ingredient

/**
 * Ingredient listing adapter.
 *
 * @property ingredientClicked
 * @constructor Create empty Ingredient listing adapter
 */
class IngredientListingAdapter(private val ingredientClicked: (Ingredient) -> Unit) :
    RecyclerView.Adapter<IngredientListingAdapter.IngredientListingViewHolder>() {

    private val ingredientListing: MutableList<Ingredient> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientListingViewHolder {
        return IngredientListingViewHolder.invoke(parent)
            .apply {
                itemView.setOnClickListener {
                    val ingredientSelected = ingredientListing[adapterPosition]
                    ingredientClicked(ingredientSelected)
                }
            }
    }

    override fun onBindViewHolder(holder: IngredientListingViewHolder, position: Int) {
        val ingredient = ingredientListing[position]
        holder.bindIngredient(ingredient)
    }

    override fun getItemCount(): Int = ingredientListing.size

    /**
     * Load ingredient listing.
     *
     * @param newIngredients
     */
    fun loadIngredientListing(newIngredients: List<Ingredient>) {
        val oldSize = ingredientListing.size
        ingredientListing.clear()
        notifyItemRangeRemoved(0, oldSize)
        ingredientListing.addAll(newIngredients)
        notifyItemRangeInserted(0, newIngredients.size)
    }

    /**
     * Ingredient listing view holder.
     *
     * @property binding
     * @constructor Create empty Ingredient listing view holder
     */
    class IngredientListingViewHolder(private val binding: IngredientItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind ingredient.
         *
         * @param ingredient
         */
        fun bindIngredient(ingredient: Ingredient) {
            binding.ingredientName.text = ingredient.strIngredient
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): IngredientListingViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = IngredientItemBinding.inflate(inflater, parent, false)
                return IngredientListingViewHolder(itemBinding)
            }
        }
    }
}
