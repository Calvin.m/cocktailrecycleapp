package com.rave.mealmodule.view.ingredientslisting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.mealmodule.R
import com.rave.mealmodule.databinding.FragmentIngredientListingBinding
import com.rave.mealmodule.viewmodel.IngredientListingViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Ingredient listing fragment.
 *
 * @constructor Create empty Ingredient listing fragment
 */
@AndroidEntryPoint
class IngredientListingFragment : Fragment() {

    private val ingredientListingViewModel by viewModels<IngredientListingViewModel>()
    private var _binding: FragmentIngredientListingBinding? = null
    private val ingredientListingAdapter by lazy {
        IngredientListingAdapter { ingredient ->
            val args = bundleOf("ingredientName" to ingredient.strIngredient)
            findNavController().navigate(
                resId = R.id.action_ingredientListingFragment_to_mealsFragment,
                args
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentIngredientListingBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvIngredients.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@IngredientListingFragment.ingredientListingAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ingredientListingViewModel.state.observe(viewLifecycleOwner) { state ->
            ingredientListingAdapter.loadIngredientListing(state.ingredientList)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
