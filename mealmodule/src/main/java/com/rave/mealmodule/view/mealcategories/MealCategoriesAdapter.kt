package com.rave.mealmodule.view.mealcategories

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rave.mealmodule.databinding.CategoryItemBinding
import com.rave.mealmodule.model.entity.MealCategory

/**
 * Meal categories adapter.
 *
 * @property categoryClicked
 * @constructor Create empty Meal categories adapter
 */
class MealCategoriesAdapter(private val categoryClicked: (MealCategory) -> Unit) :
    RecyclerView.Adapter<MealCategoriesAdapter.MealCategoriesViewHolder>() {

    private val mealCategories: MutableList<MealCategory> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealCategoriesViewHolder {
        return MealCategoriesViewHolder.invoke(parent)
            .apply {
                itemView.setOnClickListener {
                    val categorySelected = mealCategories[adapterPosition]
                    categoryClicked(categorySelected)
                }
            }
    }

    override fun onBindViewHolder(holder: MealCategoriesViewHolder, position: Int) {
        val category = mealCategories[position]
        holder.bindMealCategory(category)
    }

    override fun getItemCount(): Int = mealCategories.size

    /**
     * Load meal categories.
     *
     * @param newCocktails
     */
    fun loadMealCategories(newCocktails: List<MealCategory>) {
        val oldSize = mealCategories.size
        mealCategories.clear()
        notifyItemRangeRemoved(0, oldSize)
        mealCategories.addAll(newCocktails)
        notifyItemRangeInserted(0, newCocktails.size)
    }

    /**
     * Meal categories view holder.
     *
     * @property binding
     * @constructor Create empty Meal categories view holder
     */
    class MealCategoriesViewHolder(private val binding: CategoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind mealCategory.
         *
         * @param mealCategory
         */
        @SuppressLint("SetTextI18n")
        fun bindMealCategory(mealCategory: MealCategory) {
//            binding.cocktailCategoryName.text = cocktail.strCategory
            binding.mealCategoryName.text = mealCategory.strCategory
            binding.mealCategoryDesc.text = "${mealCategory.strCategoryDescription.split(".", "(")[0]}..."
            binding.imageView.load(mealCategory.strCategoryThumb)
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): MealCategoriesViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = CategoryItemBinding.inflate(inflater, parent, false)
                return MealCategoriesViewHolder(itemBinding)
            }
        }
    }
}
