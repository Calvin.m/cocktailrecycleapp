package com.rave.mealmodule.view.mealcategories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.mealmodule.R
import com.rave.mealmodule.databinding.FragmentMealCategoriesBinding
import com.rave.mealmodule.viewmodel.MealCategoriesViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meal categories fragment.
 *
 * @constructor Create empty Meal categories fragment
 */
@AndroidEntryPoint
class MealCategoriesFragment : Fragment() {
    private val mealCategoriesViewModel by viewModels<MealCategoriesViewModel>()
    private var _binding: FragmentMealCategoriesBinding? = null
    private val mealCategoriesAdapter by lazy {
        MealCategoriesAdapter { category ->
            val args = bundleOf("categoryName" to category.strCategory)
            findNavController().navigate(
                resId = R.id.action_mealCategoriesFragment_to_mealsFragment,
                args
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentMealCategoriesBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvCategories.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@MealCategoriesFragment.mealCategoriesAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mealCategoriesViewModel.state.observe(viewLifecycleOwner) { state ->
            mealCategoriesAdapter.loadMealCategories(state.mealCategoriesList)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
