package com.rave.mealmodule.view.mealcategories

import com.rave.mealmodule.model.entity.MealCategory

/**
 * Meal categories state.
 *
 * @property isLoading
 * @property mealCategoriesList
 * @constructor Create empty Meal categories state
 */
data class MealCategoriesState(
    val isLoading: Boolean = false,
    val mealCategoriesList: List<MealCategory> = emptyList()
)
