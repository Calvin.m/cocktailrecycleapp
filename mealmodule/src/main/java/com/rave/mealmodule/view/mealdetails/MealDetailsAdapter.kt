package com.rave.mealmodule.view.mealdetails

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rave.mealmodule.databinding.MealDetailsItemBinding
import com.rave.mealmodule.model.entity.MealDetails

/**
 * Meal details adapter.
 *
 * @property mealClicked
 * @constructor Create empty Meal details adapter
 */
class MealDetailsAdapter(private val mealClicked: (MealDetails) -> Unit) :
    RecyclerView.Adapter<MealDetailsAdapter.MealDetailsViewHolder>() {

    private val mealDetails: MutableList<MealDetails> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealDetailsViewHolder {
        return MealDetailsViewHolder(parent).apply {
            itemView.setOnClickListener {
                val mealSelected = mealDetails[adapterPosition]
                mealClicked(mealSelected)
            }
        }
    }

    override fun onBindViewHolder(holder: MealDetailsViewHolder, position: Int) {
        holder.bindMealDetails(mealDetails[position])
    }

    override fun getItemCount(): Int = mealDetails.size

    /**
     * Load meal details.
     *
     * @param newMealDetails
     */
    fun loadMealDetails(newMealDetails: List<MealDetails>) {
        val oldSize = mealDetails.size
        mealDetails.clear()
        notifyItemRangeRemoved(0, oldSize)
        mealDetails.addAll(newMealDetails)
        notifyItemRangeInserted(0, newMealDetails.size)
    }

    /**
     * AreaDTO details view holder.
     *
     * @property binding
     * @constructor Create empty AreaDTO details view holder
     */
    class MealDetailsViewHolder(private val binding: MealDetailsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind meal details.
         *
         * @param meal
         */
        fun bindMealDetails(meal: MealDetails) {
            binding.mealDetailsName.text = meal.strMeal
            binding.mealDetailsImage.load(meal.strMealThumb)
            binding.mealDetailsIngredients.text = """
                - ${meal.strMeasure1} ${meal.strIngredient1}
                - ${meal.strMeasure2} ${meal.strIngredient2}
                - ${meal.strMeasure3} ${meal.strIngredient3}
                - ${meal.strMeasure4} ${meal.strIngredient4}
                - ${meal.strMeasure5} ${meal.strIngredient5}
                - ${meal.strMeasure6} ${meal.strIngredient6}
                - ${meal.strMeasure7} ${meal.strIngredient7}
                - ${meal.strMeasure8} ${meal.strIngredient8}
                - ${meal.strMeasure9} ${meal.strIngredient9}
                - ${meal.strMeasure10} ${meal.strIngredient10}
                - ${meal.strMeasure11} ${meal.strIngredient11}
                - ${meal.strMeasure12} ${meal.strIngredient12}
                - ${meal.strMeasure13} ${meal.strIngredient13}
                - ${meal.strMeasure14} ${meal.strIngredient14}
                - ${meal.strMeasure15} ${meal.strIngredient15}
                - ${meal.strMeasure16} ${meal.strIngredient16}
                - ${meal.strMeasure17} ${meal.strIngredient17}
                - ${meal.strMeasure18} ${meal.strIngredient18}
                - ${meal.strMeasure19} ${meal.strIngredient19}
                - ${meal.strMeasure20} ${meal.strIngredient20}
            """.trimIndent().split("-  ")[0]
            binding.mealDetailsInstructions.text = meal.strInstructions
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): MealDetailsViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = MealDetailsItemBinding.inflate(inflater, parent, false)
                return MealDetailsViewHolder(itemBinding)
            }
        }
    }
}
