package com.rave.mealmodule.view.mealdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.mealmodule.databinding.FragmentMealDetailsBinding
import com.rave.mealmodule.model.remote.MealEntity
import com.rave.mealmodule.viewmodel.MealDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meal details fragment.
 *
 * @constructor Create empty Meal details fragment
 */
@AndroidEntryPoint
class MealDetailsFragment : Fragment() {
    private val mealDetailsViewModel by viewModels<MealDetailsViewModel>()
    private var _binding: FragmentMealDetailsBinding? = null
    private val fromHomeScreen by lazy { arguments?.getString("fromHomeScreen") }
    private val mealDetailsAdapter by lazy {
        MealDetailsAdapter { meal ->
            if (fromHomeScreen == "fromHomeScreen") {
                mealDetailsViewModel.deletedFavMeal(
                    MealEntity(
                        idMeal = meal.idMeal,
                        strMeal = meal.strMeal,
                        strMealThumb = meal.strMealThumb,
                        strInstructions = meal.strInstructions,
                        strIngredient1 = meal.strIngredient1,
                        strIngredient2 = meal.strIngredient2,
                        strIngredient3 = meal.strIngredient3,
                        strIngredient4 = meal.strIngredient4,
                        strIngredient5 = meal.strIngredient5,
                        strIngredient6 = meal.strIngredient6,
                        strIngredient7 = meal.strIngredient7,
                        strIngredient8 = meal.strIngredient8,
                        strIngredient9 = meal.strIngredient9,
                        strIngredient10 = meal.strIngredient10,
                        strIngredient11 = meal.strIngredient11,
                        strIngredient12 = meal.strIngredient12,
                        strIngredient13 = meal.strIngredient13,
                        strIngredient14 = meal.strIngredient14,
                        strIngredient15 = meal.strIngredient15,
                        strIngredient16 = meal.strIngredient16,
                        strIngredient17 = meal.strIngredient17,
                        strIngredient18 = meal.strIngredient18,
                        strIngredient19 = meal.strIngredient19,
                        strIngredient20 = meal.strIngredient20,
                        strMeasure1 = meal.strMeasure1,
                        strMeasure2 = meal.strMeasure2,
                        strMeasure3 = meal.strMeasure3,
                        strMeasure4 = meal.strMeasure4,
                        strMeasure5 = meal.strMeasure5,
                        strMeasure6 = meal.strMeasure6,
                        strMeasure7 = meal.strMeasure7,
                        strMeasure8 = meal.strMeasure8,
                        strMeasure9 = meal.strMeasure9,
                        strMeasure10 = meal.strMeasure10,
                        strMeasure11 = meal.strMeasure11,
                        strMeasure12 = meal.strMeasure12,
                        strMeasure13 = meal.strMeasure13,
                        strMeasure14 = meal.strMeasure14,
                        strMeasure15 = meal.strMeasure15,
                        strMeasure16 = meal.strMeasure16,
                        strMeasure17 = meal.strMeasure17,
                        strMeasure18 = meal.strMeasure18,
                        strMeasure19 = meal.strMeasure19,
                        strMeasure20 = meal.strMeasure20
                    )
                )
            } else {
                mealDetailsViewModel.addFavMeal(
                    MealEntity(
                        idMeal = meal.idMeal,
                        strMeal = meal.strMeal,
                        strMealThumb = meal.strMealThumb,
                        strInstructions = meal.strInstructions,
                        strIngredient1 = meal.strIngredient1,
                        strIngredient2 = meal.strIngredient2,
                        strIngredient3 = meal.strIngredient3,
                        strIngredient4 = meal.strIngredient4,
                        strIngredient5 = meal.strIngredient5,
                        strIngredient6 = meal.strIngredient6,
                        strIngredient7 = meal.strIngredient7,
                        strIngredient8 = meal.strIngredient8,
                        strIngredient9 = meal.strIngredient9,
                        strIngredient10 = meal.strIngredient10,
                        strIngredient11 = meal.strIngredient11,
                        strIngredient12 = meal.strIngredient12,
                        strIngredient13 = meal.strIngredient13,
                        strIngredient14 = meal.strIngredient14,
                        strIngredient15 = meal.strIngredient15,
                        strIngredient16 = meal.strIngredient16,
                        strIngredient17 = meal.strIngredient17,
                        strIngredient18 = meal.strIngredient18,
                        strIngredient19 = meal.strIngredient19,
                        strIngredient20 = meal.strIngredient20,
                        strMeasure1 = meal.strMeasure1,
                        strMeasure2 = meal.strMeasure2,
                        strMeasure3 = meal.strMeasure3,
                        strMeasure4 = meal.strMeasure4,
                        strMeasure5 = meal.strMeasure5,
                        strMeasure6 = meal.strMeasure6,
                        strMeasure7 = meal.strMeasure7,
                        strMeasure8 = meal.strMeasure8,
                        strMeasure9 = meal.strMeasure9,
                        strMeasure10 = meal.strMeasure10,
                        strMeasure11 = meal.strMeasure11,
                        strMeasure12 = meal.strMeasure12,
                        strMeasure13 = meal.strMeasure13,
                        strMeasure14 = meal.strMeasure14,
                        strMeasure15 = meal.strMeasure15,
                        strMeasure16 = meal.strMeasure16,
                        strMeasure17 = meal.strMeasure17,
                        strMeasure18 = meal.strMeasure18,
                        strMeasure19 = meal.strMeasure19,
                        strMeasure20 = meal.strMeasure20
                    )
                )
            }
        }
    }
    private val mealName by lazy { arguments?.getString("mealName") }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (mealName != null) {
            mealDetailsViewModel.fetchMealDetails(mealName ?: "")
        } else {
            mealDetailsViewModel.fetchFavorites()
        }
        return FragmentMealDetailsBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvMealDetails.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@MealDetailsFragment.mealDetailsAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mealDetailsViewModel.state.observe(viewLifecycleOwner) { state ->
            mealDetailsAdapter.loadMealDetails(state.mealDetailsList)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
