package com.rave.mealmodule.view.mealdetails

import com.rave.mealmodule.model.entity.MealDetails

/**
 * Meal details state.
 *
 * @property isLoading
 * @property mealDetailsList
 * @constructor Create empty Meal details state
 */
data class MealDetailsState(
    val isLoading: Boolean = false,
    val mealDetailsList: List<MealDetails> = emptyList()
)
