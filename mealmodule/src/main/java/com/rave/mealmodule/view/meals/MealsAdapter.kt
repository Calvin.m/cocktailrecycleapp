package com.rave.mealmodule.view.meals

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rave.mealmodule.databinding.MealItemBinding
import com.rave.mealmodule.model.entity.Meal

/**
 * Meals adapter.
 *
 * @property mealClicked
 * @constructor Create empty Meals adapter
 */
class MealsAdapter(
    private val mealClicked: (Meal) -> Unit
) :
    RecyclerView.Adapter<MealsAdapter.MealsViewHolder>() {

    /**
     * Meals view holder.
     *
     * @property binding
     * @constructor Create empty Meals view holder
     */
    class MealsViewHolder(private val binding: MealItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind meal.
         *
         * @param meal
         */
        fun bindMeal(meal: Meal) {
            binding.mealName.text = meal.strMeal
//            binding.mealName.text = meal.strMeal
            binding.imageView.load(meal.strMealThumb)
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): MealsViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = MealItemBinding.inflate(inflater, parent, false)
                return MealsViewHolder(itemBinding)
            }
        }
    }

    private val meals: MutableList<Meal> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealsViewHolder {
        return MealsViewHolder.invoke(parent)
            .apply {
                itemView.setOnClickListener {
                    val mealSelected = meals[adapterPosition]
                    mealClicked(mealSelected)
                }
            }
    }

    override fun onBindViewHolder(holder: MealsViewHolder, position: Int) {
        holder.bindMeal(meals[position])
    }

    override fun getItemCount(): Int = meals.size

    /**
     * Load meals.
     *
     * @param newMeals
     */
    fun loadMeals(newMeals: List<Meal>) {
        val oldSize = meals.size
        meals.clear()
        notifyItemRangeRemoved(0, oldSize)
        meals.addAll(newMeals)
        notifyItemRangeInserted(0, newMeals.size)
    }
}
