package com.rave.mealmodule.view.meals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.mealmodule.R
import com.rave.mealmodule.databinding.FragmentMealsBinding
import com.rave.mealmodule.viewmodel.MealsViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meals fragment.
 *
 * @constructor Create empty Meals fragment
 */
@AndroidEntryPoint
class MealsFragment : Fragment() {

    private val mealsViewModel by viewModels<MealsViewModel>()
    private var _binding: FragmentMealsBinding? = null
    private val mealsAdapter by lazy {
        MealsAdapter { meal ->
            val args = bundleOf("mealName" to meal.strMeal)
            findNavController().navigate(
                resId = R.id.action_mealsFragment_to_mealDetailsFragment,
                args
            )
        }
    }
    private val categoryName by lazy { arguments?.getString("categoryName") }
    private val areaName by lazy { arguments?.getString("areaName") }
    private val ingredientName by lazy { arguments?.getString("ingredientName") }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (categoryName != null) {
            mealsViewModel.fetchMeals(categoryName ?: "")
        } else if (areaName != null) {
            mealsViewModel.fetchMealsByArea(areaName ?: "")
        } else if (ingredientName != null) {
            mealsViewModel.fetchMealsByIngredient(ingredientName ?: "")
        }
        return FragmentMealsBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvMeals.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@MealsFragment.mealsAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mealsViewModel.state.observe(viewLifecycleOwner) { state ->
            mealsAdapter.loadMeals(state.mealsList)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
