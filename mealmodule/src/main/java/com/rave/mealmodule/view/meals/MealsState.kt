package com.rave.mealmodule.view.meals

import com.rave.mealmodule.model.entity.Meal

/**
 * Meals state.
 *
 * @property isLoading
 * @property mealsList
 * @constructor Create empty Meals state
 */
data class MealsState(
    val isLoading: Boolean = false,
    val mealsList: List<Meal> = emptyList()
)
