package com.rave.mealmodule.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.mealmodule.model.MealRepo
import com.rave.mealmodule.view.arealisting.AreaListingState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Area listing view model.
 *
 * @property repo
 * @constructor Create empty Area listing view model
 */
@HiltViewModel
class AreaListingViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {

    private val _state = MutableLiveData(AreaListingState())
    val state: LiveData<AreaListingState> get() = _state

    /**
     * Fetch areas.
     *
     */
    fun fetchAreas() {
        viewModelScope.launch {
            _state.value = _state.value?.copy(isLoading = true)
            viewModelScope.launch {
                val list = repo.getAreasListing()
                _state.value = _state.value?.copy(isLoading = false, areaListing = list)
            }
        }
    }
}
