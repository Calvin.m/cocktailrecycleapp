package com.rave.mealmodule.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.mealmodule.model.MealRepo
import com.rave.mealmodule.model.entity.Ingredient
import com.rave.mealmodule.view.ingredientslisting.IngredientListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Ingredient listing view model.
 *
 * @property repo
 * @constructor Create empty Ingredient listing view model
 */
@HiltViewModel
class IngredientListingViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {

    private val _state = MutableLiveData(IngredientListState())
    val state: LiveData<IngredientListState> get() = _state

    init {
        fetchIngredients()
    }

    private fun fetchIngredients() {
        viewModelScope.launch {
            _state.isLoading(true)
            val list = repo.getIngredientsListing()
            _state.isSuccess(list)
        }
    }

    private fun MutableLiveData<IngredientListState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<IngredientListState>.isSuccess(ingredientList: List<Ingredient>) {
        value = value?.copy(isLoading = false, ingredientList = ingredientList)
    }
}
