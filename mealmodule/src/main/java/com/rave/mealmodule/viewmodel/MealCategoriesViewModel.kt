package com.rave.mealmodule.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.mealmodule.model.MealRepo
import com.rave.mealmodule.model.entity.MealCategory
import com.rave.mealmodule.view.mealcategories.MealCategoriesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Meal categories view model.
 *
 * @property repo
 * @constructor Create empty Meal categories view model
 */
@HiltViewModel
class MealCategoriesViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {

    private val _state = MutableLiveData(MealCategoriesState())
    val state: LiveData<MealCategoriesState> get() = _state

    init {
        fetchDrinkCategories()
    }

    private fun fetchDrinkCategories() {
        viewModelScope.launch {
            _state.isLoading(true)
            viewModelScope.launch {
                val list = repo.getMealCategories()
                _state.isSuccess(list)
            }
        }
    }

    private fun MutableLiveData<MealCategoriesState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<MealCategoriesState>.isSuccess(mealCategoriesList: List<MealCategory>) {
        value = value?.copy(isLoading = false, mealCategoriesList = mealCategoriesList)
    }
}
