package com.rave.mealmodule.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.mealmodule.model.MealRepo
import com.rave.mealmodule.model.entity.MealDetails
import com.rave.mealmodule.model.remote.MealEntity
import com.rave.mealmodule.view.mealdetails.MealDetailsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Meal details view model.
 *
 * @property repo
 * @constructor Create empty Meal details view model
 */
@HiltViewModel
class MealDetailsViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {

    private val _state = MutableLiveData(MealDetailsState())
    val state: LiveData<MealDetailsState> get() = _state

    /**
     * Add fav meal.
     *
     * @param meal
     */
    fun addFavMeal(meal: MealEntity) = viewModelScope.launch {
        _state.isLoading(true)
        repo.addMeal(meal)
        _state.isLoading(false)
    }

    /**
     * Deleted fav meal.
     *
     * @param meal
     */
    fun deletedFavMeal(meal: MealEntity) = viewModelScope.launch {
        _state.isLoading(true)
        repo.deleteMeal(meal)
        _state.isLoading(false)
    }

    /**
     * Fetch meal details.
     *
     * @param meal
     */
    fun fetchMealDetails(meal: String) {
        viewModelScope.launch {
            _state.isLoading(true)
            viewModelScope.launch {
                val list = repo.getMealDetails(meal)
                // todo - if size > 1 then handle differently
                _state.isSuccess(list)
            }
        }
    }

    /**
     * Fetch favorites.
     *
     */
    fun fetchFavorites() {
        viewModelScope.launch {
            _state.isLoading(true)
            val list = repo.getFavoritedMeals()
            val goodList = list.map { it.toMapMeal() }
            _state.isSuccess(goodList)
        }
    }

    private fun MealEntity.toMapMeal(): MealDetails {
        return MealDetails(
            dateModified = "",
            idMeal = idMeal,
            strArea = "",
            strCategory = "",
            strCreativeCommonsConfirmed = "",
            strDrinkAlternate = "",
            strImageSource = "",
            strIngredient1,
            strIngredient10,
            strIngredient11,
            strIngredient12,
            strIngredient13,
            strIngredient14,
            strIngredient15,
            strIngredient16,
            strIngredient17,
            strIngredient18,
            strIngredient19,
            strIngredient2,
            strIngredient20,
            strIngredient3,
            strIngredient4,
            strIngredient5,
            strIngredient6,
            strIngredient7,
            strIngredient8,
            strIngredient9,
            strInstructions,
            strMeal = strMeal,
            strMealThumb,
            strMeasure1,
            strMeasure10,
            strMeasure11,
            strMeasure12,
            strMeasure13,
            strMeasure14,
            strMeasure15,
            strMeasure16,
            strMeasure17,
            strMeasure18,
            strMeasure19,
            strMeasure2,
            strMeasure20,
            strMeasure3,
            strMeasure4,
            strMeasure5,
            strMeasure6,
            strMeasure7,
            strMeasure8,
            strMeasure9,
            strSource = "",
            strTags = "",
            strYoutube = ""
        )
    }

    private fun MutableLiveData<MealDetailsState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<MealDetailsState>.isSuccess(mealDetailsList: List<MealDetails>) {
        value = value?.copy(isLoading = false, mealDetailsList = mealDetailsList)
    }
}
