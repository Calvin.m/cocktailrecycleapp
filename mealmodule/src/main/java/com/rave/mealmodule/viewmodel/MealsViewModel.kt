package com.rave.mealmodule.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.mealmodule.model.MealRepo
import com.rave.mealmodule.model.entity.Meal
import com.rave.mealmodule.view.meals.MealsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Meals view model.
 *
 * @property repo
 * @constructor Create empty Meals view model
 */
@HiltViewModel
class MealsViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {

    private val _state = MutableLiveData(MealsState())
    val state: LiveData<MealsState> get() = _state

    /**
     * Fetch meals.
     *
     * @param category
     */
    fun fetchMeals(category: String) {
        viewModelScope.launch {
            _state.isLoading(true)
            viewModelScope.launch {
                val list = repo.getMeals(category)
                _state.isSuccess(list)
            }
        }
    }

    /**
     * Fetch meals by area.
     *
     * @param area
     */
    fun fetchMealsByArea(area: String) {
        viewModelScope.launch {
            _state.isLoading(true)
            viewModelScope.launch {
//                val list = repo.getMeals(category)
                val list = repo.getMealsByArea(area)
                _state.isSuccess(list)
            }
        }
    }

    /**
     * Fetch meals by ingredient.
     *
     * @param ingredient
     */
    fun fetchMealsByIngredient(ingredient: String) {
        viewModelScope.launch {
            _state.isLoading(true)
            viewModelScope.launch {
                val list = repo.getMealsByIngredient(ingredient)
                _state.isSuccess(list)
            }
        }
    }

    /**
     * Is loading.
     *
     * @param isLoading
     */
    private fun MutableLiveData<MealsState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    /**
     * Is success.
     *
     * @param mealsList
     */
    private fun MutableLiveData<MealsState>.isSuccess(mealsList: List<Meal>) {
        value = value?.copy(isLoading = false, mealsList = mealsList)
    }
}
