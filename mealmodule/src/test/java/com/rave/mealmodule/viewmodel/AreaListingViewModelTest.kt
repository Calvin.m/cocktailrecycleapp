package com.rave.mealmodule.viewmodel

import com.rave.mealmodule.model.MealRepo
import com.rave.mealmodule.model.entity.Area
import com.rave.mealmodule.testUtils.CoroutinesTestExtension
import com.rave.mealmodule.testUtils.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.function.Executable

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class, CoroutinesTestExtension::class)
internal class AreaListingViewModelTest {
    private val mockRepo = mockk<MealRepo>()
    private val mockVM: AreaListingViewModel = AreaListingViewModel(mockRepo)

    @Test
    @DisplayName("Test fetching areas")
    fun testFetchAreas() = runTest {
        // When
        val state = mockVM.state.value

        // Then
        assertAll(
            Executable {
                assertTrue(state?.isLoading == false, "checking loading")
                assertTrue(state?.areaListing?.isEmpty() == true, "checking list")
            }
        )
    }

    @Test
    fun testValueChange() = runTest {
        val expChange = listOf(Area("Test"))
        // Given
        coEvery { mockRepo.getAreasListing() } coAnswers { expChange }
        // When
        mockVM.fetchAreas()
        // Then
        val state = mockVM.state.value
        assertTrue(state?.isLoading == false)
        assertTrue(state?.areaListing?.size == 1)
        assertTrue(state?.areaListing?.first()?.strArea == expChange.first().strArea)
    }
}
